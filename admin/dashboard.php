<?php
//session_start();
include("../dbconnect.php");
ob_start();
session_start();

if(isset($_SESSION['id'])){}
else{
	// Start the session
	$_SESSION["id"] = uniqid();
	//$_SESSION["cart"] = 0;
}

		//if already logged in by cache memory
if(isset($_SESSION['admin'])){
	$admin=$_SESSION['admin'];
			
	}

else
{	
	unset($_SESSION['admin']);
	header("Location: index.php?action=relogin");
}
?>

<!DOCTYPE html>
<html lang = "en" itemscope itemtype="http://schema.org/Article">
	 <head>
		<title>Dashboard</title>
		<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="../images/favicon.ico" type="image/x-icon">

		<!--materialize-->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!-- Compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">

		<!-- Compiled and minified JavaScript -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">  
		
		
		<script>	
			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				constrainWidth: false, // Does not change width of dropdown to that of the activator
				hover: true, // Activate on hover
				gutter: 0, // Spacing from edge
				belowOrigin: false, // Displays dropdown below the button
				alignment: 'left' // Displays dropdown with edge aligned to the left of button
				stopPropagation: false // Stops event propagation
			});
		</script>
		<style>
		.dashboardicons{
			font-size: 200%;
			float:left;
			margin-left:1%; 
			margin-bottom:2%;
			padding:1%; 
			width:30%;
			color: white; 
			font-family: 'Pacifico';
			text-align:center;
		}
		</style>
	 </head>
		
	<body>
	<?php
	    include("navbar.php");
    ?>
	

		<div class="tab-content" style="padding-left: 2cm;">

		<!--HOME DATA-->
		<div class="tab-pane fade in active" id="home" name="home" style="position:relative; padding-top:2cm;padding-right:40px;">
		<?php
			$agents=$db->query("select count(*) as total from admin_login where type='Admin';");
			$totagents = $agents->fetch_assoc();

			$mines=$db->query("select DISTINCT count(organisation) as total from categories;");
			$totmines=$mines->fetch_assoc();

			$price=$db->query("select count(*) as total from price_list where date='".date("Y-m-d")."';");
			$totprice=$price->fetch_assoc();
		?>

		<span style="font-size:250%">Dashboard </span>
		<br><br>
		<div class="row">
		<div class="red dashboardicons">Number of Agents: <?php echo $totagents['total']; ?></div>
		<div class="red dashboardicons">Number of Mines: <?php echo $totmines['total']; ?></div>
		<div class="red dashboardicons">Prices to be Updated today: <?php echo $totprice['total']; ?></div>
		</div>
	</div>
	<!--TO SHOW NOTIFICATIONS-->
                <div style="font-size:250%;clear:both">Notifications</div><br>
                <div id="notifications" style="clear:both">
                
                <?php
				if(date("l")=="Monday"){
				echo "<div class='red dashboardicons'>Weekly prices need to be updated today</div>";
				}
				if(date("d")=="1"){
				echo "<div class='red dashboardicons'>Monthly Prices need to be updated today</div>";
				}
                ?>
                </div>

            </div>

    </body>
</html>