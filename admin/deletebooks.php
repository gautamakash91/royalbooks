<?php


require('../dbconnect.php');
if(isset($_POST['updatebooks'])){
    $id=$_POST['check'];
    header("location:updatebooks.php?id=".$id); 
}
?>
<!DOCTYPE html>
  <html>
    <head>
      <title>Delete Books</title>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<!-- Compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">

		<!-- Compiled and minified JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>

		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
		<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
        <script>
        $(document).ready(function() {
            $('#booking').DataTable( {
                
            } );
        } );
        </script>

      <script>  
      $(document).ready(function(){
        $(".button-collapse").sideNav();
      });
	  
	  $(document).ready(function() {
    $('select').material_select();
  });
      
      </script>
	  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
    </head>

    <body>

      <!-- Navbar goes here -->

      <?php
      require("navbar.php");
      ?>

<?php
     
  if(isset($_POST['deletebooks'])){
        
       $id=$_POST['check'];
        
       
            if(mysqli_query($db,"DELETE FROM books WHERE id='".$id."'")){
                echo "
                <script>
                Materialize.toast('Deleted Successfully', 4000);
                </script>
                ";	
				header("Location: deletebooks.php");
            }else{
                echo "
                <script>
                Materialize.toast('Could not be Deleted', 4000);
                </script>
                ";
            }
        }  
		   
 ?>

<div class="container">
    <div class="row">
    <div class="col s10">

    <form method="post" action="deletebooks.php" >
        
<table id="booking" class="bordered" width="70%">
    <thead>
        <tr>
		 <th>Select</th>  
		 <th>ID</th>
            <th>Book Name</th>        
            <th>Author Name</th>
			<th>Condition</th> 
			<th>MRP</th>        
            <th>Price</th>
			<th>Type</th>     
            			
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT * FROM books";
            $res = $db->query($query);
            while(($row = $res->fetch_assoc())!=null){
        ?>
        <tr>
            <td align="center">
            <p>
              <input type="radio" name="check" id="<?php echo $row['id']; ?>" value="<?php echo $row['id']; ?>" />
              <label for="<?php echo $row['id']; ?>"></label>
            </p>
            </td>   
			 <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['BookName']; ?></td>
			<td><?php echo $row['AuthorName']; ?></td>
			
			<td><?php echo $row['Mrp']; ?></td>
			<td><?php echo $row['New_Price']; ?></td>
			<td><?php echo $row['Old_Price']; ?></td>
			<td><?php echo $row['type']; ?></td>
			
        </tr> 
        <?php  } ?>
    </tbody>
</table>
					
        
        <center><input type="submit" class="red white-text btn" name="deletebooks" value="Delete"/>
		<input type="submit" class="red white-text btn" name="updatebooks" value="Update"/></center>
		
    </form>
    </div>
</div>
</div>

          
    </body>
  </html>
 