<?php
session_start();	
require('../dbconnect.php');

    if(isset($_POST['username'])){
      $username=$db->real_escape_string($_POST['username']);
			$password=$db->real_escape_string($_POST['password']);
		
			$login_sql="SELECT * FROM admin_login WHERE username='".$username."' AND password='".$password."'";
			$login_query=mysqli_query($db, $login_sql);
        
			if(mysqli_num_rows($login_query)>0) {
					$login_rs=mysqli_fetch_assoc($login_query);
					$_SESSION['admin']=$login_rs['username'];
					$_SESSION['username']=$username;
					$_SESSION['email']=$login_rs['email'];
					$_SESSION['type']=$login_rs['type'];
					
                    header("Location: dashboard.php");
            } 
			else{
			
					header("Location: index.php?error=login");
				}
				
	}

?>


<html lang = "en">
   
   <head>
      <title>Login Page</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
      <!-- Compiled and minified JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
      <link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
     <style>
body {
    background-image: url("back.jpeg");
}</style>	 
                
   </head>
 
    <body>
      <div>
        <font color="white"> 
          <center>
            <img src="../images/rb.jpg" height="200">
          </center> 
        </font>
        <br><br>
        
          <div class="container black" style="opacity:0.8" id="login">
            <form class = "col s12" action = "#" method = "post">
            <div class="container">
                <div class="row">
                    <div class="input-field col s12">
                      <input style="color:white" type="text" id="username1" class="validate" name="username" required>
                      <label style="color:white" for="username1">Username</label>
                    </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input style="color:white" type="password" id="password1" class="validate" name="password" required>
                    <label style="color:white" for="password1">Password</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                      <?php   
                            if(isset($_GET['error'])){
                        ?>
							  	    <span class="error">Wrong credentials try again!</span>
                        <?php 
                            }
                        ?>
                  </div>
                </div>
                <button class = "waves-effect waves-light btn" type = "submit" name = "login">Login</button>
            </form>  
          </div>  
          </div>
        </div> 
   </body>
</html>

<style>
    #login{
        padding: 2%;
        border: 2px solid #008080;
        border-radius: 5px;
    }
    .error{
        font-size: 1.1em;
        color: white;
    }
</style>