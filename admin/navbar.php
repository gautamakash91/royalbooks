<script>
 $(document).ready(function() {
    $('select').material_select();
     
     $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    }
  );
      $(document).ready(function(){
    $('.collapsible').collapsible();
  });
       
  });
    
    
     $(function(){
        $('.my-btn').sideNav({
          edge: 'left', // Choose the horizontal origin
          closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
          draggable: true // Choose whether you can drag to open on touch screens
        }
        );
      });
      
</script>

<nav class="white" role="navigation">
    <div class="nav-wrapper">
     <a href="dashboard.php" class="brand-logo"><img src="../images/rb.jpg" style="width:5%"></a>
     <a href="" class="brand-logo center black-text">ROYAL BOOKS</a>
     <ul id="nav-mobile" class="right hide-on-med-and-down">
       <!-- <li>
         
            <?php 
				
								if(isset($_SESSION['admin']))
							{ 
								echo "<a  href=\"index.php\" style=\"color:black;\">Welcome ".$_SESSION['admin']."</a>"."</li><li><a class=\"btn red white-text\" href=\"index.php?action=logout\" id=\"myHref\">Logout</a>";
							}
							else
							{
								echo "<a class=\"btn red white-text\" data-toggle=\"modal\"  href=\"index.php\">Login</a>";
							}
				
							?>

         </li>-->
         <li><a data-activates="slide-out" class="my-btn btn transparent grey-text" href="#"><i class="material-icons">menu</i></a>
                <ul id="slide-out" class="side-nav">
                      <li><a class="waves-effect" href="dashboard.php">Dashboard</a></li>
					  <li class="divider"></li>
                      <ul class="collapsible" data-collapsible="accordion" style="color:black;">
                          <li>
                            <a class="collapsible-header ">Books<i class="material-icons">keyboard_arrow_down</i></a>
							
                            <div class="collapsible-body">
                                  <ul>
									 
                                      <li><a href="addbooks.php">Add Books</a></li>
									  <li class="divider"></li>
                                      <li><a href="deletebooks.php">View/Delete Books</a></li>
									 
                                  </ul>
                            </div>
                          </li>
							<li class="divider"></li>
                          
                            
                          <li><a class="waves-effect" href="settings.php">Settings</a></li>
						  <li class="divider"></li>
                          <li><a class="waves-effect" href="logout.php">Logout</a></li>
						  <li class="divider"></li>
                    </ul>
              </ul>

         </li>
         
      </ul>
        
    </div>
  </nav>

