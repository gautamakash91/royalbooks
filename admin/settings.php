<?php
	//session_start();
	include("../dbconnect.php");
	ob_start();
	session_start();

	if(isset($_SESSION['id'])){}
	else{
		// Start the session
		$_SESSION["id"] = uniqid();
	}

	//if already logged in by cache memory
	if(isset($_SESSION['admin'])){
		$admin=$_SESSION['admin'];
		$email=$_SESSION['email'];
		$type=$_SESSION['type'];			
	}
	else
	{	
		unset($_SESSION['admin']);
		header("Location: index.php?action=relogin");
	}
?>

<!DOCTYPE html>
<html lang = "en" itemscope itemtype="http://schema.org/Article">

	 <head>
		<title></title><link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
		<!-- Latest compiled and minified CSS -->
			<link rel="stylesheet" href="css/social.css"> 
			<!--materialize-->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!-- Compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">

		<!-- Compiled and minified JavaScript -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">  
			
			
		<script>
			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				constrainWidth: false, // Does not change width of dropdown to that of the activator
				hover: true, // Activate on hover
				gutter: 0, // Spacing from edge
				belowOrigin: false, // Displays dropdown below the button
				alignment: 'left' // Displays dropdown with edge aligned to the left of button
				stopPropagation: false // Stops event propagation
			});	
		</script>
		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
		<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
		<script src="js/yrss.min.js"></script>

		<script>
			$(document).ready( function () {
				$('#pricetable').DataTable();
			});
		</script>
	 </head>
	<body>
	<?php
		include("navbar.php");
	?>

		<div id = "main">
			<div class="form-group well" style="position:relative;width:60%;left:20%; margin-top: 50px;">
				<strong style="font-size:150%"><center>Settings</center></strong><br><br>
				<form class = "form-signin" role = "form" action = "" method = "post">
					<center>
					<br>
					<?php		
					if($type=="Super_Admin"){
						echo "<a href='addadmin.php'>Click to Add New Admin</a>";
					}
					?>
					<br><br>
					Name : <label><?php echo $admin;?></label>
					<br>
					Email : <label><?php echo $email;?></label><br><br>
					Password : <a href="changepassword.php">Change Password</a>
					<br><br>
					</center>

					<table class="responsive-table striped">
						<thead>
							<tr>
								<th>Select</th>        
								<th>ID</th>
								<th>Username</th>
								<th>Email</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$query = "SELECT id,username,email FROM admin_login where type='Admin'";
								$res = $db->query($query);
								while(($row = $res->fetch_assoc())!=null){
							?>
							<tr>
								<td>
									<input value="<?php echo $row['id'];?>" name="checked[]" type="checkbox" id="<?php echo $row['id'];?>">
									<label for="<?php echo $row['id'];?>"></label>
								</td>        
								<td><?php echo $row['id']; ?></td>
								<td><?php echo $row['username']; ?></td>
								<td><?php echo $row['email']; ?></td>
							</tr> 
							<?php  } ?>
						</tbody>
					</table>
					<center>
						<input type="submit" class="btn" name="delete" value="Remove">
					</center>
				</form>			
			</div>	
		</div>
			
		<?php
		include("../footer.html");
?>		
	</body>
</html>

<style>
.form-group{
		border: 1px solid lightgray;
		padding: 50px;
		margin: 10px;
}
.col-sm-6{
		padding: 10px;
}
</style>


<?php
if(isset($_POST['delete'])){
            $idArr = $_POST['checked'];
			
            foreach($idArr as $id){
                mysqli_query($conn,"DELETE FROM admin_login WHERE id='".$id."'");
            }
}	
?>


		