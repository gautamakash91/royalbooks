<?php 
require('dbconnect.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>

 
  <title>Royal Books</title>
  <?php include('header.php');?>
  <script>     
    $(document).ready(function(){
      $('.slider').slider();
      $('.modal').modal();
    });
  </script>


    <?php

    if(isset($_GET['item'])){
      echo "
      <script>
        $(document).ready(function(){
        Materialize.toast('Item Added To cart', 7000 ,'red'); // 4000 is the duration of the toast
      });
      </script>
      ";
    }

    ?>

  
</head>
<body>
  <?php
  include('navbar.php');
  ?>
    <div class="container">
    <div class="row">
    <div style="margin:auto;height:50%;width:50%; background-color:#aeeaae;color:#1f7a1f;padding:10%">
    <h4>YOUR ORDER HAS BEEN CONFIRMED</h4>  
    </div>
    </div>
    </div>
  
  <?php
    include('footer.html'); 
  ?> 
  </body>
</html>