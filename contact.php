<!DOCTYPE html>
  <html>
    <head>
      <title>Royal Books</title>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <link rel="stylesheet" href="css/social.css">
        <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>

      <style>

        html {
              background: url(images/topbanner2.jpg) no-repeat center center fixed;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;
            }

        .navbtn:hover{
          background-color: #212121;
          color:#9ccc65;
        }

      </style>

      <script>  
      $(document).ready(function(){
        $(".button-collapse").sideNav();
      });
      
      </script>
	  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>

    <body>

       <!-- Navbar goes here -->


       <?php
       include('navbar.php');
       ?>

                <div class="container">
                <div class="col s12 card-panel lighten-5">
                <div class="container">
                <div class="row">
                    <div class="col s12 center"><span class="flow-text">Write to us!</span></div>
                </div>
                    <form class="col s12">
                      <div class="row">
                        <div class="input-field col s5 ">
                          <i class="material-icons prefix">account_circle</i>
                          <input id="name" name="name" type="text" class="validate">
                          <label for="name">Name</label>
                        </div>
                        <div class="col s2"></div>
                        <div class="input-field col s5 ">
                          <i class="material-icons prefix">phone</i>
                          <input id="phone" name="phone" type="tel" class="validate">
                          <label for="phone">Telephone</label>
                        </div>
                      </div>
                      <div class="input-field col s12 ">
                      <i class="material-icons prefix">email</i>
                        <input id="email" name="email" type="email" class="validate">
                        <label for="email" data-error="wrong" data-success="right">Email</label>
                      </div><br>
                      <div class="input-field col s12 ">
                         <i class="material-icons prefix">mode_edit</i>
                         <textarea id="message" name="message" class="materialize-textarea"></textarea>
                         <label for="message">Message</label>
                      </div>
                      <div class="row center">
                        <button class="btn waves-effect waves-light z-depth-5" style="margin-top: 50px; background-color:#cc3333" type="submit" id="submit" name="submit">Send
                          <i class="material-icons right">send</i>
                        </button>
                      </div>
                    </form>
                </div>
                </div>
                </div>
    <div class="container white" style="padding:3%;">
       
        <div class="row" align="center">
        <?php
    include('footer.html'); 
  ?> 
    </div>

<!--footer-->

    </body>
  </html>



  <?php

	$_POST['name'] = "";
	$_POST['phone'] = "";
	$_POST['email'] = "";
	$_POST['message'] = "";

	if(isset($_POST['submit']))
	{
	if($_POST['name'] != "" || $_POST['phone'] != "" || $_POST['email'] != "" || $_POST['message'] != "")
	{

	$subject = "Enquiry";
	$txt = $_POST['name']." ".$_POST['email']." ".$_POST['phone'];
	$headers = "From: Royal Books contact form";
	$txt = $_POST['name']." ".$_POST['email']." ".$_POST['phone']." ".$_POST['message'];

	mail("gautamakash91@gmail.com",$subject,$txt,$headers);
  echo "
    <script>  
      Materialize.toast('Message sent', 4000);
    </script>
    ";
	}

	}
  ?>