<?php

// $dbconnect = array(

//     'server' => 'localhost',
//     'user' => 'root',
//     'pass' => '',
//     'name' => 'royalbooks'
// );

$dbconnect = array(
    'server' => '127.0.0.1',
    'user' => 'root',
    'pass' => 'password',

    'name' => 'royalbooks'
);

$db= new mysqli(
$dbconnect['server'],
$dbconnect['user'],
$dbconnect['pass'],
$dbconnect['name']
);

if($db -> connect_errno >0){
    echo "<br><br><h2>RoyalBooks cant be reached right now! :( </h2><br> Error details:" .$db ->connect_error;
    exit;
}


?>
