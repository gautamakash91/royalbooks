<?php 
session_start();
include("dbconnect.php"); //connecting to database in sql

?>
  


<!DOCTYPE html>
<html lang="en">
<head>

  
  <title>Browse Products</title>
		<?php include('header.php');?>

    <script type="text/javascript" language="javascript">
        $( document ).ready(function(){

            $(".button-collapse").sideNav();//mobile screen menu init

            $('.carousel').carousel(); //carousel init

            $('.carousel-slider').slider({full_width: true});//slider init

            $(".dropdown-button").dropdown();
        });

    </script>
  
 
  
</head>
<body>
  <?php
include('navbar.php'); ?> 
  
<div class='row'>
<!--Items-->
<?php
    

		//$query = "select * from '".$_GET['category']."' WHERE subcategory='".$_GET['subcategory']."'";
    $query = "SELECT * from products where type='".$_GET['type']."'";
    //echo $query;
            $res = $db->query($query);
    while(($row = $res->fetch_assoc())!=null){
      if($row['New_Price']==0){
        $newprice="Sold Out";  
      }else{
        $newprice=$row['New_Price'];
      }

      if($row['Old_Price']==0){
        $oldprice="Sold Out";  
      }else{
        $oldprice=$row['Old_Price'];
      }
  echo "

  <div class='col s6 m2'>
        <div class='card large'>
        <div class='card-image'>
            <!-- front content -->
            <a href='product.php?id=".$row['id']."&type=books'><img src=\"images\books/".trim($row['id']).".jpg\" style='width:100%;height:300px' alt='Card image cap'></a>
        </div>
        <div class='card-content'>
            <form method='post' action='product.php?id=".$row['id']."&type=".$_GET['type']."'>
            <p class='card-text blue-grey-text'><b>".$row['BookName']."</b></p>
            <p class='card-text'>".$row['AuthorName']."</p>
                                            
            <p class='card-text'>MRP: ".$row['Mrp']."</p>
            <p class='card-text'>New Book: ".$newprice."</p>
            <p class='card-text'>Old Book: ".$oldprice."</p>
                        
            <input type='text' name='name' value=".$row['BookName']." style='display:none'>
            <input type='text' name='author' value=".$row['AuthorName']." style='display:none'>
            <input type='number' name='price' value=".$row['New_Price']." style='display:none'>
            
            <!-- <input type='submit' class='btnAddAction' value='Add to Cart' name=\"cart\">
            <input type='submit' class='btnAddAction' value='Add to Wishlist' name=\"wish\">-->
                
            </form>
        </div>
        
        </div>
    </div>
  
          
  ";
		}
?>

  </div>

          
    <?php
    include('footer.html'); 
  ?> 
    
    </body>
</html>