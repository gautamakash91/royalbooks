<?php
session_start();
include("dbconnect.php"); //connecting to database in sql
?>

<!DOCTYPE html>
<html lang = "en">

   <head>
      <title>Forgot Password</title>
      <?php include('header.php');?>
   </head>

    
    
  <body>
      <nav class="white" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo"><i class="large material-icons"><img src="images/logo1.jpg" style=" width:6%;border-radius:20%;left:6%;position:relative"></i>ArtHardy</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="wishlist.php" class="waves-effect waves-light btn btn-large"><i class="material-icons left">assignment</i>Wishlist</a></li>
		<li><a href="cart.php" class="waves-effect waves-light btn btn-large"><i class="material-icons left">assessment</i>Cart</a></li>
		
      </ul>

    
    </div>
  </nav>
      
    <div id = "main">
        <div class="form-group well" style="position:relative;width:40%;left:30%;text-align:center; margin-top: 50px;">
          <strong style="font-size:150%"><center>Forgot Password</center></strong><hr>
            
            
            <p><b>Enter your 
                <span><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="The one you used to create the account!">Registered Email id</a>
                </span> 
                You will receive an Email with a new Password. Kindly check you inbox.</b>
            </p><br>
            
            
          <form class = "form-signin" role = "form" action = "password_reset.php" method = "post">
              <div class="input-field col s6">
                              <i class="material-icons prefix">account_circle</i>
                              <input id="icon_prefix" type="text" class="validate" name = "email" required>
                              <label for="icon_prefix">Email Id</label>
            </div>
             
              <button class = "waves-effect waves-light btn" type = "submit" name = "login">Recover Account</button><br>
              <br>
              <a class="" href="login.php.php">Remember Your password?</a>
              <h5>Or</h5>
              <?php include("google-login.php");?>
              <br>
              
              
              <?php
	//FACEBOOK LOGIN

	# Start the session 
	//session_start();
	
	# Autoload the required files
	require_once __DIR__ . '/vendor/autoload.php';

	# Set the default parameters
	$fb = new Facebook\Facebook([
	  'app_id' => '573650092844396',
	  'app_secret' => 'a2a24406c42c12b3d581e63a614be649',
	  'default_graph_version' => 'v2.5',
	]);
	$redirect = 'http://www.arthardy.com/login.php';


	# Create the login helper object
	$helper = $fb->getRedirectLoginHelper();

	# Get the access token and catch the exceptions if any
	try {
	  $accessToken = $helper->getAccessToken();
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  // When Graph returns an error
	  echo 'Graph returned an error: ' . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  // When validation fails or other local issues
	  echo 'Facebook SDK returned an error: ' . $e->getMessage();
	  exit;
	}

	# If the 
	if (isset($accessToken)) {
	  	// Logged in!
	 	// Now you can redirect to another page and use the
  		// access token from $_SESSION['facebook_access_token'] 
  		// But we shall we the same page

		// Sets the default fallback access token so 
		// we don't have to pass it to each request
		$fb->setDefaultAccessToken($accessToken);

		try {
		  $response = $fb->get('/me?fields=email,name');
		  $userNode = $response->getGraphUser();
		}catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

            $name=$userNode->getName();
            $id = $userNode->getId();
            $email = $userNode->getProperty('email');
        
            $login_sql="SELECT * FROM user WHERE user_id='".$id."' ;";
				$login_query=mysqli_query($db, $login_sql);
			if(mysqli_num_rows($login_query)>0) {
					$login_rs=mysqli_fetch_assoc($login_query);
					$_SESSION['admin']=$login_rs['student_id']; // setting admin variable to user id
                    header("Location: index.php");
            } 
			else{
                //IF NEW USER
			     $login_sql="INSERT INTO `cart` VALUES ('$id', ' ', '$email', '$name');";
				    $login_query=mysqli_query($db, $login_sql);
                if(mysqli_num_rows($login_query)>0) {
					$login_rs=mysqli_fetch_assoc($login_query);
					$_SESSION['admin']=$login_rs['student_id']; // setting admin variable to user id
                    header("Location: index.php");
                    } 
                else{
                    echo "error";
                }
				}
		// Print the user Details
		/*echo "Welcome !<br><br>";
		echo 'Name: ' . $userNode->getName().'<br>';
		echo 'User ID: ' . $userNode->getId().'<br>';
		echo 'Email: ' . $userNode->getProperty('email').'<br><br>';

		$image = 'https://graph.facebook.com/'.$userNode->getId().'/picture?width=200';
		echo "Picture<br>";
		echo "<img src='$image' /><br><br>";*/
		
	}else{
		$permissions  = ['email'];
		$loginUrl = $helper->getLoginUrl($redirect,$permissions);
		echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
	}

?>
              
              
          </form>
            
        </div>
      </div>
      <?php
    include('footer.html'); 
  ?> 
   </body>
</html>

<style>
.form-group{
    border: 1px solid lightgray;
    padding: 50px;
    margin: 10px;
}
.col-sm-6{
    padding: 10px;
}
</style>

 <script>
      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
      };
    </script>