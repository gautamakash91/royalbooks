<?php 
require('dbconnect.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>

 <link href="https://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet">
 
  <title>Royal Books</title>
  <?php include('header.php');?>
  <script>     
    $(document).ready(function(){
      $('.slider').slider();
      $('.modal').modal();
    });
  </script>  
</head>
<body>
  <?php
  include('navbar.php');

  if(isset($_GET['item'])){

    if($_GET['item']=='added'){
      echo "
      <script>
        $(document).ready(function(){
        Materialize.toast('Item Added To cart', 7000 ,'red'); // 4000 is the duration of the toast
      });
      </script>
      ";
    }else{
      echo "
      <script>
        $(document).ready(function(){
        Materialize.toast('Item could not be added to cart', 7000 ,'red'); // 4000 is the duration of the toast
      });
      </script>
      ";
    }
    
  }
  ?>
    
    <div class="slider">
    <ul class="slides">
      <li>
        <img src="images/slider2.jpeg"> <!-- random image -->
        <div class="caption left-align">
        <h3>BOOKS</h3>
          <h5 class="light grey-text text-lighten-3">We deliver your favourite books to you so you can indulge in your fantasies.</h5>
        </div>
      </li>
      <li>
        <img src="images/slider1.jpeg"> <!-- random image -->
        <div class="caption center-align">
          <h3>GIFTS</h3>
          <h5 class="light grey-text text-lighten-3">Every gift from a friend is a wish for happiness</h5>
        </div>
      </li>
      <li>
        <img src="images/slider3.jpeg"> <!-- random image -->
        <div class=" caption right-align">
          <center><h3 class="black-text">STATIONARY</h5><center>
          <h5 class="light black-text text-lighten-3"></h5>
        </div>
      </li>
    </ul>
  </div>

  <br><br>

  <div class="container">
    <div class="center col s12">
      <h5 class="_15">About RoyalBooks</h5>
      <hr>
      <p style="text-align:justify;">
        RoyalBooks aims to be the one stop solution for all types of books and gift items.
        We are young and dynamic, striving hard to fulfil customer needs and aspirations. We aspire to be the leading bookstore online, providing the best at cheap prices.
        Books are a uniquely portable magic that we deliver at your door step. If you are stacked with books you never read anymore, Royal books is the fast and easy way  
<b>Sell your old books online.</b>
      </p>
      <a href="about.php">Click to know more</a>
    </div>
  </div>
  <br><br>
    <div class="row">
    
      <div class="col s12 m4">
      <a class="modal-trigger" href="#modal1">
          <div class="card">
            <div class="card-image">
              <img src="images/books.jpg" style="height:250px">
              <span class="card-title" style="width:100%; font-size:6vw"><b>Books</b></span>
            </div>
          </div>
          </a>
      </div>
      
      <div class="col s12 m4">
      <a href="stationary.php">
          <div class="card">
            <div class="card-image">
              <img src="images/stationary.jpeg" style="height:250px">
              <span class="card-title" style="width:100%; font-size:6vw"><b>Stationary</b></span>
            </div>
          </div>
          </a>
      </div>

      
      <div class="col s12 m4">
      <a class="modal-trigger" href="#">
          <div class="card" style="height:200px">
            <div class="card-image">
              <img src="images/gifts.jpg" style="height:250px">
              <span class="card-title"> 
                <span  style="width:50%; font-size:6vw"><b>Gifts</b></span>
                Coming Soon
              </span>
            </div>
          </div>
          </a>
      </div>
      

    </div>
    
<!-- Books -->
<!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
    
                    
  <?php
      $query1 = "SELECT DISTINCT type FROM products where type != 'STATIONARY'";
      $res = $db->query($query1);
      while(($row = $res->fetch_assoc())!=null){
        echo "
        <center><ul><b><a href='detail.php?type=".$row['type']."'>".strtoupper($row['type'])."</a></b></ul></center><hr>
        ";
    } 
    ?>

    </div>
  </div>

  
  <?php
    include('footer.html'); 
  ?> 
  </body>
</html>