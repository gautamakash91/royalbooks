<?php
   
    ob_start();
    session_start();
    require('dbconnect.php');
    require 'PHPMailer/PHPMailerAutoload.php';

    if($_POST['pay']=='payonline') {

        $id=$_SESSION['id'];
        $name=mysqli_real_escape_string($db,$_POST['cus_name']);
        $add=$_POST['address'];
        $email=$_POST['email'];
        $total=$_POST['total'];
        $phone=mysqli_real_escape_string($db,$_POST['phone']);
        $sql = "INSERT INTO customer_details VALUES('$id', '$name', '$phone', '$email', '$add', $total, 'Online Payment');";
        if ($db->query($sql)) {
            echo "success";
        } else {
            echo "Error updating record: ";
        }
    
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                    array("X-Api-Key:d7a2e94dd7ebc27bdf6a96655e658117",
                          "X-Auth-Token:e2009791f3fad7ee6a2078454e3a89f2"));
        $payload = Array(
            'purpose' => 'Royal Books Purchase',
            'amount' => $total,
            'phone' => $phone,
            'buyer_name' => $name,
            'redirect_url' => 'http://www.royalbooks.in',
            'send_email' => false,
            'webhook' => 'http://www.royalbooks.in/webhook.php/',
            'send_sms' => false,
            'email' => $email,
            'allow_repeated_payments' => false
        );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
        $response = curl_exec($ch);
        curl_close($ch); 
        
        echo $response;

        $json_decode = json_decode($response, true);
    
        $long_url = $json_decode['payment_request']['longurl'];
        header('Location:'.$long_url);

    }
    else {
        //CASH ON DELIVERY
        $id=$_SESSION['id'];
        $name=$_POST['cus_name'];
        $add=$_POST['address'];
        $email=$_POST['email'];
        $total=$_POST['total'];
        $phone=$_POST['phone'];

$content = '<html>
<body>
    <div>
        <hr style="height: 1px;
            border: 0;
            border-top: 5px solid #ff0000;">

        <div class="header">
            <h1 style="color: #666699; padding: 20px;">Royal Books</h1>
        </div>

        <hr style="height: 1px;
            border: 0;
            border-top: 5px solid #e0e0eb;">
        
        <center>
            <h2 style="color: #666699;">Dear Customer</h2>
        </center>

        <center>
            <h3 style="color: #666699;">Thanks for ordering from Royal Books</h3>
            <h4 style="color: #666699;">This is an email confirmation of your order.</h4>
        </center>

        <span style="color: #666699;">Name:</span> '.$name.'<br>
        <span style="color: #666699;">Address:</span> '.$add.'<br>
        <span style="color: #666699;">Phone:</span> '.$phone.'<br> 
        <span style="color: #666699;">Payment:</span> Cash on delivery<br> 
        
        <hr style="height: 1px;
            border: 0;
            border-top: 2px solid #e0e0eb;">

        <div>
            <h4 style="float: left; margin-left: 10px">ORDER ID:</h4><br>
            <h4 style="float: right; margin-right: 10px">Placed on:'.date("d-m-Y").'</h4>
        </div>
        <br>

        <center>
        <table style="width:70%">
        <thead>
            <tr>
                <th>Book Name</th>
                <th>Author Name</th>
                <th>Quantity</th>
                <th>Type</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>'; 

        $query = "select B.id as productid, B.BookName as bookname, B.AuthorName as authorname, C.quantity as quantity, C.type as type, C.total_price as price from products as B, cart as C where C.userid='".$_SESSION["id"]."' and B.id=C.productid";

        $result = $db->query($query);
            
        while (($row = $result->fetch_assoc() )!=null) {
                $content = $content."
                <tr>
                    <td>".$row['bookname']."</td>
                    <td>".$row['authorname']."</td>
                    <td>".$row['quantity']."</td>
                    <td>".$row['type']."</td>
                    <td>".$row['price']."</td>
                </tr>";
        }


        $content = $content."
        <tr>
            <td>Total</td>
            <td></td>
            <td></td>
            <td></td>
            <td>".$total."</td>
        </tr>
        </tbody>
        </table>

        </div>

    </body>
</html>
        ";

    // Payment was successful, mark it as successful in your database.
    // You can acess payment_request_id, purpose etc here. 

    //SEND EMAIL TO ROYAL BOOKS
    $mail = new PHPMailer;
    $mail->isSendmail();
    //Set who the message is to be sent from
    $mail->setFrom('info@royalbooks.in', 'Royal Books');
    //Set an alternative reply-to address
    $mail->addReplyTo('info@royalbooks.in', 'Royal Books');
    //Set who the message is to be sent to
    $mail->addAddress('singhatul54321@gmail.com','kumar.rahulsingh834@gmail.com',"Royal Books"); 
    //Set the subject line
    $mail->Subject = 'Order Confirmation';
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //Replace the plain text body with one created manually
    $mail->Body    = $content;
    $mail->AltBody = "Your order has been confirmed";
    
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        
    }


    //SEND EMAIL TO CUSTOMER
    $mail = new PHPMailer;
    $mail->isSendmail();
    //Set who the message is to be sent from
    $mail->setFrom('info@royalbooks.in', 'Royal Books');
    //Set an alternative reply-to address
    $mail->addReplyTo('info@royalbooks.in', 'Royal Books');
    //Set who the message is to be sent to
    $mail->addAddress($email, $name);
    //Set the subject line
    $mail->Subject = 'Order Confirmation';
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //Replace the plain text body with one created manually
    $mail->Body    = $content;
    $mail->AltBody = "Your order has been confirmed";
    
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        $query = "delete from cart where userid='".$_SESSION["id"]."'";
        $result = $db->query($query);
        header("location:confirmed.php");
    }

    }
?>
    