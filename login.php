<?php
session_start();
include("dbconnect.php");
?>

<!DOCTYPE html>
<html lang = "en" itemscope itemtype="http://schema.org/Article">

   <head>
      <title>Login</title><link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
      <!-- Latest compiled and minified CSS -->
       <link rel="stylesheet" href="css/social.css"> 
       <!--materialize-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

      <!-- Compiled and minified JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
          
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      
      <script>
          
           $('.dropdown-button').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false, // Does not change width of dropdown to that of the activator
              hover: true, // Activate on hover
              gutter: 0, // Spacing from edge
              belowOrigin: false, // Displays dropdown below the button
              alignment: 'left' // Displays dropdown with edge aligned to the left of button
              stopPropagation: false // Stops event propagation
            }
          );
      </script>
   </head>

    
    <?php
    if(isset($_GET['wish'])){				
	
	?>
   <script>
    $(document).ready(function(){
    Materialize.toast('You need to login first to access your Wishlist', 20000 ,'red') // 4000 is the duration of the toast
  });</script>
    <?php
        
}
    ?>
    
  <body>
      <nav class="white" role="navigation">
    <div class="nav-wrapper">
      <a id="logo-container" href="index.php" class="brand-logo"><img src="images/rb.jpg" height="60px" width="70px" style="margin-left: 20px"></a>
     <!-- <ul class="right hide-on-med-and-down">
        <li><a href="cart.php" class="waves-effect blue darken-3 btn">Cart<i class="material-icons right">assessment</i></a></li>
        <li><a class='dropdown-button waves-effect blue darken-3 btn' href="#dropdown1" data-activates='dropdown1'><i class="material-icons right">contacts</i>Profile</a>
          <!-- Dropdown Structure 
          <ul id='dropdown1' class='dropdown-content'>
            <li><a href="wishlist.php">Wishlist</a></li>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </li>
      </ul>-->
      
    </div>
  </nav>
 <!-- <nav>
    <div class="nav-wrapper">
      <form>
        <div class="input-field grey darken-2">
          <input id="search" type="search" placeholder="BEGIN YOUR SEARCH HERE" required>
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav>-->
      
    <div id = "main">
        <div class="form-group well" style="position:relative;width:40%;left:30%;text-align:center; margin-top: 50px;">
          <strong style="font-size:150%"><center>Login</center></strong><br><br>
          <form class = "form-signin" role = "form" action = "config.php" method = "post">
              <div class="input-field col s6">
                <i class="material-icons prefix">account_circle</i>
                <input id="icon_prefix" type="text" class="validate" name = "username" required>
                <label for="icon_prefix">UserName</label>
              </div>
              <div class="input-field col s6"> 
                <i class="material-icons prefix">verified_user</i>
                <input id="icon_prefix" type="password" class="validate" name = "password" required>
                <label for="icon_prefix">Password</label>
              </div>
              <button class = "waves-effect waves-light btn" type = "submit" name = "login">Login</button><br>
              <!-- FORM FOR NORMAL LOGIN ENDS HERE-->
              <br>
              <!-- FORGOT PASSWORD LINK-->
              <a class="" href="forgotpass.php">forgot password ?</a>
              <h5>Or</h5>

              
              <a href="flogin.php"><img src="images/facebook.png"></a>
              
              <div><a href="glogin.php"><img style="height:80px;" src="images/glogin.png" alt=""/></a></div>
              
          </form>
            
        </div>
      </div>
      
      <?php
    include('footer.html'); 
  ?> 
      
   </body>
</html>

<style>
.form-group{
    border: 1px solid lightgray;
    padding: 50px;
    margin: 10px;
}
.col-sm-6{
    padding: 10px;
}
</style>

    
    
    