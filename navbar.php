

 <nav class="grey darken-3" role="navigation">
    <div class="nav-wrapper">
      <a id="logo-container" href="index.php"><img src="images/rb.jpg" height="60px" width="60px" style="margin-left: 20px"></a>
      <a href="#" class="brand-logo white-text center">Royal Books</a>
      <ul class="right">
        <li style="background-color: grey"><a style="background-color:#424242" href="cart.php" class="waves-effect"><i class="material-icons" style="font-size:30px">add_shopping_cart</i></a></li>
        <li style="background-color: grey"><a style="background-color:#424242" href="contact.php" class="waves-effect">Contact us</a></li>
        <!-- <li style="margin-right: 5px; background-color: grey"><a class="dropdown-button waves-effect" href="#dropdown1" data-activates='dropdown1'><i class="material-icons">contacts</i></a> -->
          <!-- Dropdown Structure -->
          <ul id='dropdown1' class='dropdown-content'>
            <!-- <li><a href="wishlist.php">Wishlist</a></li> -->
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </li>
      </ul>
      
    </div>
  </nav>

  <nav>
    <div class="nav-wrapper">
      <form action="redirect.php" method="post">
        <div class="input-field white">
          <input id="tag" type="search" name="search" onkeyup="showResult(this.value)" placeholder="BEGIN YOUR SEARCH HERE" autocomplete="off" required>
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav>
  

<div id="livesearch"></div>

<script>
function showResult(str) {
  if (str.length==0) { 
    document.getElementById("livesearch").innerHTML="";
    document.getElementById("livesearch").style.border="0px";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("livesearch").innerHTML=this.responseText;
      document.getElementById("livesearch").style.border="1px solid #A5ACB2";
    }
  }
  xmlhttp.open("GET","autocomplete.php?q="+str,true);
  xmlhttp.send();
}
</script>
