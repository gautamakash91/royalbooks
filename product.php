<?php 
session_start();
include("dbconnect.php"); //connecting to database in sql

?>
  
<!DOCTYPE html>
<html lang="en">
<head>

  <title>Browse Products</title>
  <?php include('header.php');?>
  <script type="text/javascript" language="javascript">
        $( document ).ready(function(){
            $(".button-collapse").sideNav();//mobile screen menu init
            $('.carousel').carousel(); //carousel init
            $('.carousel-slider').slider({full_width: true});//slider init
            $(".dropdown-button").dropdown();
        });
    </script>
  
 
  
</head>
<body>
   <?php 
   include("navbar.php");
   ?>
  

<div class='container'>
  <div class='row'>
<!--Items-->
<?php

	$query = "SELECT * from products WHERE id ='".$_GET['id']."' ;";
    //echo $query;
    $res = $db->query($query);
    while(($row = $res->fetch_assoc())!=null){
  
        ?>
      <div class="col s12 m6" align="center"> 
        <img src="images/books/<?php echo $row['id'];?>.jpg" alt='Books' style="padding:2%;width:100%;">
      </div>
            <div class="col s12 m6">
                    <h5><?php echo $row['BookName']; ?></h5>
                <br>
                    <h6>Author Name :<?php echo $row['AuthorName']?></h6>
                    <hr>
                <br>
                <br>
                 <form action='addcart.php' method='post' >
                        <input type='text' name='proid' value="<?php echo $_GET['id'];?>" style='display:none'>
                        <input type='text' name='type' value="<?php echo $row['type'];?>" style='display:none'>
                        <div class="row">
						 <div class="col s12"><h5 id="tag"><b>MRP: </b><span> &#8377; &nbsp;<?php echo $row['Mrp'];?></span></h5>
                         </div>
						  
                         <div class="col s6">
                         NEW BOOK
                         </div>

                         <div class="col s6">
                         OLD BOOK
                         </div>
                         <?php 
                         if($row['New_Price']!=0){
						 echo "<div class='col s6'>			  
                            <input name='type' type='radio' id='test1' value='New_".$row['New_Price']."' required />
                            <label class='teal-text' for='test1'> &#8377; &nbsp;".$row['New_Price']."</label>
                            </div>";
						 }else if($row['New_Price']==0){
                            echo "<div class='col s6'>			  
                               <input disabled name='type' type='radio' id='test1' required />
                               <label for='test1'>Sold Out</label>
                               </div>";
                            }

						if($row['Old_Price']!=0){ 
                             echo "<div class='col s6'>
                            <input name='type' type='radio' id='test2' value='Old_".$row['Old_Price']."' required />
                            <label class='teal-text' for='test2'> &#8377; &nbsp; ".$row['Old_Price']."</label>
                             </div>";
                        }else if($row['Old_Price']==0){ 
                            echo "<div class='col s6'>
                           <input disabled name='type' type='radio' id='test2' required />
                           <label for='test2'>Sold Out</label>
                            </div>";
                       }
                        
                        ?>
						
						  <div class="input-field col s6">
                              <input id="icon_prefix" type='number' name='quantity' value='1' min="1" max="3" class="validate">
                              <label for="icon_prefix">Quantity</label>
                            </div>
                    </div>
                     <br>
                         <div class="row">
                             <div class="col s6">
                                    <button class="waves-effect blue darken-3 btn" type='submit' class='btnAddAction' name="cart">Add to Cart</button>
                            </div>
                            <!-- <div class="col s6">     
                                    <input class="waves-effect blue darken-3 btn" type='submit' class='btnAddAction' value='Add to Wishlist' name="wish">
                            </div> -->
                        </div>
                          
                    </form>
                
  </div>
	<?php }?>
  </div>
    </div>
	
 <?php
    include('footer.html'); 
  ?> 
    </body>
</html>

<style>
    #tag, #tag1{
        font-size: 1em;
    }
    #tag span{
        font-size: 1.5em;
        color: red;
    }
</style>