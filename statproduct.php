<?php 
session_start();
include("dbconnect.php"); //connecting to database in sql

?>
  
<!DOCTYPE html>
<html lang="en">
<head>

  <title>Browse Products</title>
  <?php include('header.php');?>
  <script type="text/javascript" language="javascript">
        $( document ).ready(function(){
            $(".button-collapse").sideNav();//mobile screen menu init
            $('.carousel').carousel(); //carousel init
            $('.carousel-slider').slider({full_width: true});//slider init
            $(".dropdown-button").dropdown();
        });
    </script>
  
 
  
</head>
<body>
   <?php 
   include("navbar.php");
   ?>
  

<div class='container'>
  <div class='row'>
<!--Items-->
<?php

	$query = "SELECT * from products WHERE id ='".$_GET['id']."' ;";
    //echo $query;
    $res = $db->query($query);
    while(($row = $res->fetch_assoc())!=null){
  
        ?>
      <div class="col s12 m6" align="center"> 
        <img src="images/stationary/<?php echo $row['id'];?>.jpg" alt='Books' style="padding:2%;width:100%">
      </div>
            <div class="col s12 m6">
                    <h5><?php echo $row['BookName']; ?></h5>
                <br>
                <br>
                 <form action='addcart.php' method='post' >
                        <input type='text' name='proid' value="<?php echo $_GET['id'];?>" style='display:none'>
                        <input type='text' name='Mrp' value="<?php echo $row['Mrp'];?>" style='display:none'>
                        <div class="row">
                        <div class="col s12"><h5 id="tag"><b>MRP: </b><span> &#8377; &nbsp;<?php echo $row['Mrp'];?></span></h5>
                        </div>
						
						  <div class="input-field col s6">
                              <input id="icon_prefix" type='number' name='quantity' value='1' min="1" max="3" class="validate">
                              <label for="icon_prefix">Quantity</label>
                            </div>
                    </div>
                     <br>
                        <div class="row">
                            <div class="col s6">
                                <button class="waves-effect blue darken-3 btn" type='submit' class='btnAddAction' name="cart">Add to Cart</button>
                            </div>
                        </div>
                           
                    </form>
                
  </div>
	<?php }?>
  </div>
    </div>
	
 <?php
    include('footer.html'); 
  ?> 
    </body>
</html>

<style>
    #tag, #tag1{
        font-size: 1em;
    }
    #tag span{
        font-size: 1.5em;
        color: red;
    }
</style>