<?php
ob_start();
session_start();
include("dbconnect.php");
require 'PHPMailer/PHPMailerAutoload.php';

$data = $_POST;
$payment_id = $data['payment_id'];
$payment_request_id = $data['payment_request_id'];

$result = $db->query("select * from customer_details where id='".$_SESSION["id"]."'");
if (($row = $result->fetch_assoc() )!=null) {
    $name = $row['cus_name'];
    $address = $row['address'];
    $payment = $row['mode'];
    $email = $row['email'];
}

$content = '<html>
<body>
    <div>
        <hr style="height: 1px;
            border: 0;
            border-top: 5px solid #ff0000;">

        <div class="header">
            <h1 style="color: #666699; padding: 20px;">Royal Books</h1>
        </div>

        <hr style="height: 1px;
            border: 0;
            border-top: 5px solid #e0e0eb;">
        
        <center>
            <h2 style="color: #666699;">Dear Customer</h2>
        </center>

        <center>
            <h3 style="color: #666699;">Thanks for ordering from Royal Books</h3>
            <h4 style="color: #666699;">This is an email confirmation of your order.</h4>
        </center>

        <span style="color: #666699;">Name:</span> '.$name.'<br>
        <span style="color: #666699;">Address:</span> '.$add.'<br>
        <span style="color: #666699;">Phone:</span> '.$phone.'<br> 
        <span style="color: #666699;">Payment:</span> Online Payment<br> 
        
        <hr style="height: 1px;
            border: 0;
            border-top: 2px solid #e0e0eb;">

        <div>
            <h4 style="float: left; margin-left: 10px">ORDER ID:</h4><br>
            <h4 style="float: right; margin-right: 10px">Placed on:'.date("d-m-Y").'</h4>
        </div>
        <br>

        <center>
        <table style="width:70%">
        <thead>
            <tr>
                <th>Book Name</th>
                <th>Author Name</th>
                <th>Quantity</th>
                <th>Type</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>'; 

        $query = "select B.id as productid, B.BookName as bookname, B.AuthorName as authorname, C.quantity as quantity, C.type as type, C.total_price as price from products as B, cart as C where C.userid='".$_SESSION["id"]."' and B.id=C.productid";
        echo $query;

        $result = $db->query($query);
            
        while (($row = $result->fetch_assoc() )!=null) {
                $content = $content."
                <tr>
                    <td>".$row['bookname']."</td>
                    <td>".$row['authorname']."</td>
                    <td>".$row['quantity']."</td>
                    <td>".$row['type']."</td>
                    <td>".$row['price']."</td>
                </tr>";
        }


        $content = $content."
        <tr>
            <td>Total</td>
            <td></td>
            <td></td>
            <td></td>
            <td>".$total."</td>
        </tr>
        </tbody>
        </table>

        </div>

    </body>
</html>
        ";


if($data['status'] == "Credit"){
    //SEND EMAIL TO ROYAL BOOKS
    $mail = new PHPMailer;
    $mail->isSendmail();
    //Set who the message is to be sent from
    $mail->setFrom('info@royalbooks.in', 'Royal Books');
    //Set an alternative reply-to address
    $mail->addReplyTo('info@royalbooks.in', 'Royal Books');
    //Set who the message is to be sent to
    $mail->addAddress('singhatul54321@gmail.com','kumar.rahulsingh834@gmail.com',"Royal Books"); 
    //Set the subject line
    $mail->Subject = 'Order Confirmation';
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //Replace the plain text body with one created manually
    $mail->Body    = $content;
    $mail->AltBody = "Your order has been confirmed";
    
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        
    }


    //SEND EMAIL TO CUSTOMER
    $mail = new PHPMailer;
    $mail->isSendmail();
    //Set who the message is to be sent from
    $mail->setFrom('info@royalbooks.in', 'Royal Books');
    //Set an alternative reply-to address
    $mail->addReplyTo('info@royalbooks.in', 'Royal Books');
    //Set who the message is to be sent to
    $mail->addAddress($email, $name);
    //Set the subject line
    $mail->Subject = 'Order Confirmation';
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //Replace the plain text body with one created manually
    $mail->Body    = $content;
    $mail->AltBody = "Your order has been confirmed";
    
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        $query = "delete from cart where userid='".$_SESSION["id"]."'";
        $result = $db->query($query);
        header("location:confirmed.php");
    }
}
else{
    // Payment was unsuccessful, mark it as failed in your database.
    // You can acess payment_request_id, purpose etc here.
// Payment was successful, mark it as successful in your database.
    // You can acess payment_request_id, purpose etc here. 
    $mail = new PHPMailer;
    $mail->isSendmail();
    //Set who the message is to be sent from
    $mail->setFrom('info@royalbooks.in', 'Royal Books');
    //Set an alternative reply-to address
    $mail->addReplyTo('info@royalbooks.in', 'Royal Books');
    //Set who the message is to be sent to
    $mail->addAddress($email, $name);
    $mail->addAddress('info@royalbooks.in',"Royal Books"); 
    //Set the subject line
    $mail->Subject = 'Order failed';
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //Replace the plain text body with one created manually
    $mail->Body    = "Your order has failed due to online payment failure";
    $mail->AltBody = "Your order has failed due to online payment failure";
    if (!$mail->send()) {
        header("location:index.php");
    } else {
        header("location:index.php");
    }
}

?>